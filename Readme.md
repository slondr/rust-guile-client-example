This is a small program that demonstrates how to use the `rust-guile` crate to embed a Scheme interpreter in your Rust program and call Rust functions from within Scheme.
